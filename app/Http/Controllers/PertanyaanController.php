<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
Use Carbon\Carbon;


class PertanyaanController extends Controller
{
    //
    public function index(){
    	$questions = DB::table('pertanyaan')->get();
    	//dd($questions);
    	return view('pertanyaan.pertanyaan',compact('questions'));
    }

    public function create(){
    	return view('pertanyaan.create');
    }

    public function store(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'judul' => 'required',
    		'isi'	=> 'required'
    	]);
    	$nows=Carbon::now()->toDateString();
    	$questions = DB::table('pertanyaan')->insert([
    		"judul" => $request["judul"],
    		"isi" => $request["isi"],
    		"tanggal_dibuat" => $nows,
    		"tanggal_diperbaharui" => $nows
    		]);
    	return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil dibuat');
    }

    public function show($id){
    	$questions = DB::table('pertanyaan')->where('id',$id)->first();
    	//dd($questions);
    	return view('pertanyaan.show',compact('questions'));
    }

    public function edit($id){
    	$questions = DB::table('pertanyaan')->where('id',$id)->first();
    	//dd($questions);
    	return view('pertanyaan.edit',compact('questions'));
    }

    public function update($id, Request $request){
    	$nows=Carbon::now()->toDateString();
    	$questions = DB::table('pertanyaan')->where('id',$id)->update([
	    		"judul" => $request["judul"],
	    		"isi" => $request["isi"],
	    		"tanggal_diperbaharui" => $nows
    		]);
    	return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil diupdate');
    }

    public function destroy($id){
    	$questions = DB::table('pertanyaan')->where('id',$id)->delete();
    	return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil dihapus');
    }
}
