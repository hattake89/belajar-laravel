@extends('admin.app')


@section('content')
<div class="card">
      <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
          <div class="alert alert-success"> {{ session('success') }}</div>
        @endif
        <a class="btn btn-primary mb-2" href="/pertanyaan/create"> Buat Pertanyaan Baru</a>
        <table id="dataPertanyaan" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diupdate</th>
            <th>User Penanya</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($questions as $key => $question)
          <tr>
            <td>{{ $key +1 }}</td>
            <td>{{ $question->judul ?? '' }}</td>
            <td>{{ $question->isi ?? ''}}</td>
            <td>{{ $question->tanggal_dibuat ?? ''}}</td>
            <td>{{ $question->tanggal_diperbaharui ?? ''}}</td>
            <td>{{ $question->profil_id ?? ''}}</td>
            <td style="display: flex;">
                <a href="/pertanyaan/{{ $question->id }}" class="btn btn-info btn-sm">Show</a> <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-default btn-sm">Edit</a> <form action="/pertanyaan/{{$question->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="7" align="center">No Data</td>
          </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diupdate</th>
            <th>User Penanya</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection
