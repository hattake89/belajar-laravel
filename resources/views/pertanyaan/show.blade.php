@extends('admin.app')

@section('content')
          <div class="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan {{$questions->id}}</h3>
              </div>
                            <!-- /.card-header -->
              <!-- form start -->
               <div class="card-body">
        <a class="btn btn-primary mb-2" href="/pertanyaan"> Kembali</a>
        <table id="detailPertanyaan" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Kolom</th>
            <th>Isi</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>ID</td>
            <td>{{ $questions->id ?? '' }}</td>
          </tr>
          <tr>
            <td>Judul</td>
            <td>{{ $questions->judul ?? '' }}</td>
          </tr>
          <tr>
            <td>Isi</td>
            <td>{{ $questions->isi ?? '' }}</td>
          </tr>
          <tr>
            <td>Tanggal Dibuat</td>
            <td>{{ $questions->tanggal_dibuat ?? '' }}</td>
          </tr>
          <tr>
            <td>Tanggal Diperbaharui</td>
            <td>{{ $questions->tanggal_diperbaharui ?? '' }}</td>
          </tr>
          </tbody>
        </table>
      </div>
              </div>
          </div>
@endsection
