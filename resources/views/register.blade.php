<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tugas Laravel - Register</title>
</head>
<body>

<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="welcome.html">
<label for="firsname">First name:</label><br>
<input type="text" name="firsname"><br><br>
<label for="lastname">Last name:</label><br>
<input type="text" name="lastname"><br><br>

<label for="gender">Gender:</label><br>
<input type="radio" id="male" name="gender" value="male">
<label for="male">Male</label><br>
<input type="radio" id="female" name="gender" value="female">
<label for="female">Female</label><br>
<input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label><br><br>

<label for="nationality">Nationality:</label><br>
<select name="nationality" id="nationality">
    <option value="indonesian">Indonesian</option>
    <option value="american">American</option>
</select><br><br>

<label>Language Spoken:</label><br>
<input type="checkbox" id="bahasa" name="bahasa" value="Bahasa Indonesia">
<label for="bahasa">Bahasa Indonesia</label><br>
<input type="checkbox" id="english" name="english" value="English">
<label for="english">English</label><br>
<input type="checkbox" id="other" name="other" value="Other">
<label for="other">Other</label><br><br>

<label>Bio:</label><br>
<textarea rows="5" id="bio" name="bio"></textarea><br>
<input type="submit" value="Sign Up" >
<form>
</body>
</html>